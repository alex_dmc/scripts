#!/usr/bin/env bash
for song in ~/Music/downloads/singles/*;
do
    artist=`exiftool -Artist "${song}"`;
    title=`exiftool -title "${song}"`;
    title=${title#*: } ;
    artist=${artist#*: } ;
    if [[ -d  ~/Music/Artists/"${artist}" ]] ;
    then
        if [[ -d  ~/Music/Artists/"${artist}"/Loosies ]] ; then
            cp  "${song}" ~/Music/Artists/"${artist}"/Loosies ;
        else
            mkdir -p ~/Music/Artists/"${artist}"/Loosies ;
            cp  "${song}" ~/Music/Artists/"${artist}"/Loosies ;
        fi
    else
        mkdir -p ~/Music/Artists/"${artist}"/Loosies ;
        cp "${song}" ~/Music/Artists/"${artist}"/Loosies ;
    fi
    song_=${song##*/}
    if [[ -f ~/Music/Artists/$artist/Loosies/$song_ ]] ; then
        exiftool "${song}" -Album -Artist -title >> ~/'success.txt';
        echo '---' >> ~/'success.txt';
    else
        exiftool "${song}" -Album -Artist -title >> ~/'fail.txt';
        echo '---' >> ~/'fail.txt';
    fi
done
