#!/bin/bash
dirs_misc=( 'art' 'gifs' 'musicians' 'memes' 'personal/2022' 'science')
drives=("drive1" "drive2" "drive3" )
for drive in "${drives[@]:0:2}"; do
    find ~/Downloads/photo_imports/personal/2022 -type f -print0 | xargs -0 cp -t /media/dank/${drive}/Pictures/personal/2022;
done
for drive in "${drives[@]:1:2}"; do
    for direc in "${dirs_misc[@]}"; do
        find ~/Downloads/photo_imports/${direc} -type f -print0 | xargs -0 cp -t /media/dank/${drive}/Pictures/${direc};
    done
done
